import React, { Component } from 'react';
import './KnowledgeHut_QuizBee_Assets/KnowledgeHut_QuizBee_Assets/assets/style.css';
import quizservice from './KnowledgeHut_QuizBee_Assets/KnowledgeHut_QuizBee_Assets/quizService/index';
import QuestionBox from './KnowledgeHut_QuizBee_Assets/KnowledgeHut_QuizBee_Assets/components/QuestionBox'
import Result from './KnowledgeHut_QuizBee_Assets/KnowledgeHut_QuizBee_Assets/components/Result';

class App extends Component {
 state={
   questionBank:[],
   score:0,
   response:0
 };
 getQuestions=()=>{
   quizservice().then(questions=>(
     this.setState({
       questionBank:questions
     })
   ))
  
 }

computeAnswer=(answer,correctAnswer)=>{
  if(answer=== correctAnswer){
  this.setState({
    // score:this.state.score + 1
    score: this.state.score + 1
  })
  
  };
  this.setState({
    response:this.state.response< 5 ? this.state.response+1:5
  })
};
playagain=()=>{
  this.getQuestions();
  this.setState({
    score:0,
    response:0,
    questionBank:[]
  })
}
 componentDidMount(){
   this.getQuestions()

 }
  render() { 
    return (  

     <div className="container">
       <div className="title">
         QuizBee
       </div>
       {this.state.questionBank.length> 0 &&  this.state.response<5 && this.state.questionBank.map(({question,answers,correct,questionId})=>
       <QuestionBox question={question} options={answers} correct={correct} questionId={questionId}
       selected={answer=> this.computeAnswer(answer,correct)}
       
       />
       
       
       ) }


       {this.state.response===5 ?        <Result score={this.state.score} playAgain={this.playagain} />:null}

     </div>
     );
  }
}
 
export default App;